<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('adverts', function (Blueprint $table) {
            $table->increments('advert_id');
            $table->string('advert_title', 100);
            $table->string('advert_details', 500);
            $table->string('advert_location',100);
            $table->string('advert_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adverts');
    }
}
