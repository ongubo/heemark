<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('user_name');
            $table->string('user_lastName');
            $table->string('email')->unique();
            $table->string('user_role', 50)->default('Student');
            $table->integer('user_verified')->default(1);
            $table->string('user_phone');
            $table->string('password', 60);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('fees', function (Blueprint $table) {
            $table->increments('fee_id');
            $table->integer('fee_student_id')->unsigned();
            $table->foreign('fee_student_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->string('fee_referenceNumber', 100);
            $table->string('fee_transactionType', 50);
            $table->decimal('fee_amount', 10, 2);
            $table->timestamps();
        });

        Schema::create('bank', function (Blueprint $table) {
            $table->increments('fee_id');
            $table->string('bank_name', 50);
            $table->integer('bank_student_id')->unsigned();
            $table->foreign('bank_student_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->string('bank_referenceNumber', 100);
            $table->string('bank_transactionNumber', 50);
            $table->decimal('bank_amount', 10, 2);
            $table->timestamps();
        });

        Schema::create('units', function (Blueprint $table) {
            $table->increments('unit_id');
            $table->integer('unit_student_id')->unsigned();
            $table->foreign('unit_student_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->string('unit_code', 20);
            $table->string('unit_name', 50);
        });
         Schema::create('courseUnits', function (Blueprint $table) {
            $table->increments('courseunit_id');
            $table->string('courseunit_code', 20);
            $table->string('courseunit_name', 50);
            $table->string('courseunit_period', 50);
        });
        Schema::create('results', function (Blueprint $table) {
            $table->increments('result_id');
            $table->integer('result_student_id')->unsigned();
            $table->foreign('result_student_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->string('result_unitCode', 20);
            $table->string('result_unitName', 50);
            $table->string('result_grade', 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('fees');
        Schema::drop('units');
        Schema::drop('results');
        Schema::drop('courseUnits');
        Schema::drop('bankTable');
    }
}
