<?php

use App\Fee;
use App\User;
use Illuminate\Database\Seeder;

require_once '/vendor/fzaninotto/Faker/src/autoload.php';

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EventTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(UnitTableSeeder::class);
        $this->call(FeeTableSeeder::class);
        $this->call(ResultTableSeeder::class);
        $this->call(CourseUnitsTableSeeder::class);
    }
}
///seeds the course units table
class CourseUnitsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('courseUnits')->delete();
        $names = array();
        $file  = file_get_contents('units.txt', "r");
        foreach (preg_split("/((\r?\n)|(\r\n?))/", $file) as $line) {
            $names[] = explode(":", $line);
        };
        foreach ($names as $name) {
            DB::table('courseUnits')->insert(
                [
                    'courseunit_code'   => $name[0],
                    'courseunit_name'   => $name[1],
                    'courseunit_period' => $name[2],
                ]
            );
        }

    }
}
////seeds the users table
class UserTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();
        DB::table('users')->delete();
        $firstName = $faker->firstName($gender = null | 'male' | 'female');
        $lastName  = $faker->lastName;
        for ($i = 0; $i < 2; $i++) {
            User::create([
                'user_name'     => $firstName,
                'user_lastName' => $faker->lastName,
                'email'         => $faker->freeEmail,
                'user_phone'    => $faker->phoneNumber,
                'password'      => Hash::make($firstName),
                'user_role'     => 'Administrator',

            ]);
        }

        for ($count = 0; $count < 20; $count++) {
            $firstName = $faker->firstName($gender = null | 'male' | 'female');
            $lastName  = $faker->lastName;
            User::create([
                'user_name'     => $firstName,
                'user_lastName' => $faker->lastName,
                'email'         => $faker->freeEmail,
                'user_phone'    => $faker->phoneNumber,
                'password'      => Hash::make($firstName),
                'user_role'     => $faker->randomElement($array = array('Finance Staff', 'Registry', 'Lecturer')),

            ]);
        }

        for ($count = 0; $count < 200; $count++) {
            $firstName = $faker->firstName($gender = null | 'male' | 'female');
            $lastName  = $faker->lastName;
            User::create([
                'user_name'     => $firstName,
                'user_lastName' => $faker->lastName,
                'email'         => $faker->freeEmail,
                'user_phone'    => $faker->phoneNumber,
                'password'      => Hash::make($firstName),
                'user_role'     => "Student",
            ]);
        }

    }

}
///seeds the fees table
class FeeTableSeeder extends Seeder
{
    public function run()
    {
        $users = User::all();
        $faker = Faker\Factory::create();
        DB::table('fees')->delete();

        foreach ($users as $user) {
            $feesCount = $faker->numberBetween($min = 2, $max = 5);
            if ($user->user_role == 'Student') {
                for ($i = 0; $i < $feesCount; $i++) {
                    Fee::create([
                        'fee_student_id'          => $user->user_id,
                        'fee_referenceNumber' => $faker->swiftBicNumber,
                        'fee_transactionType' => $faker->randomElement($array = array('RECEIPT', 'ADJUSTMENT', 'HELB LOAN')),
                        'created_at'          => $faker->dateTimeThisDecade($max = 'now'),
                        'fee_amount'          => $faker->numberBetween($min = 1000, $max = 50000),
                    ]);
                }
            }
        }
    }
}

///seeds the events table
class EventTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        DB::table('adverts')->delete();

        for ($i = 0; $i < 20; $i++) {
            DB::table('adverts')->insert(
                [
                    'advert_title'    => $faker->sentence($nbWords = 5, $variableNbWords = true),
                    'advert_details'  => $faker->realText($maxNbChars = 200, $indexSize = 2),
                    'advert_location' => $faker->streetName,
                    'advert_image'    => $faker->imageUrl('500', '400', 'city'),
                    'created_at'      => $faker->dateTimeThisDecade($max = 'now'),
                ]
            );
        }
    }
}

///seeds the units table
class UnitTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        $users = User::all();
        DB::table('units')->delete();
        $units = array(
            "BFI 3120" => "Business Finance",
            "BCD 3107" => "Object Oriented System Analysis and Design",
            "BCS 3102" => "E-Commerce Foundations",
            "BIT 3205" => "System Support",
            "BIT 3208" => "System Admnistration",
            "UCC 105"  => "Gender, Sexuality, HIV & AIDS",
            "BCS 2108" => "Introduction to Internet Technologies",
            "BMA 4204" => "Strategic Management",
            "BIT 4101" => "Information Resource Management",
            "BCS 105"  => "Data Communication",
            "BIT 2208" => "Software Engineering",
            "BSB 405"  => "Entrepreneurship",
            "BMA 1200" => "Business Communication",
            "BCS 4102" => "Computer Graphics",
            "BIT 4206" => "Accounting Information Systems");

        foreach ($users as $user) {
            if ($user->user_role == 'Student') {
                $shuffled_array = array();
                $keys           = array_keys($units);
                shuffle($keys);
                foreach ($keys as $key) {
                    $shuffled_array[$key] = $units[$key];
                }
                $count = 0;
                foreach ($shuffled_array as $key => $value) {
                    DB::table('units')->insert([
                        'unit_student_id' => $user->user_id,
                        'unit_code'  => $key,
                        'unit_name'  => $value,
                    ]);
                    $count++;
                    if ($count >= 8) {
                        break;
                    }
                }
            }
        }
    }
}

///seeds the results table
class ResultTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        DB::table('results')->delete();

        $users = User::all();
        foreach ($users as $user) {
            if ($user->user_role == 'Student') {
                $units = $user->units;
                foreach ($units as $unit) {
                    DB::table('results')->insert([
                        'result_student_id'      => $user->user_id,
                        'result_unitCode' => $unit->unit_code,
                        'result_unitName' => $unit->unit_name,
                        'result_grade'    => $faker->randomElement($array = array('A', 'B', 'C', 'D', 'E')),
                    ]);
                }
            }
        }
    }
}
