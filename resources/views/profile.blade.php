@extends('layouts.admin')
@section('title', 'Profile')
@section('content')
<section id="content">
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
		<div class="row">
			<div class="col s12 m12 l12">
				<h5 class="breadcrumbs-title">
				User Profile
				</h5>
				<ol class="breadcrumbs">
					<li>
						<a href="">
							Dashboard
						</a>
					</li>
					<li>
						<a href="#">
							User
						</a>
					</li>
					<li class="active">
						Profile
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<div class="row">
				<form class="col s12" method="POST" action="{{ url('user') }}">
				{!! csrf_field() !!}
					<div class="row">
					<div  class="input-field col s12 m6">
							<input id="name" value="{{$user->user_name}}" name="name" type="text" required class="validate">
							<label  class="active" for="name">First Name</label>
						</div>
						<div  class="input-field col s12 m6">
							<input id="lastname" value="{{$user->user_lastName}}" name="lastname" type="text" required class="validate">
							<label  class="active" for="lastname">Last Name</label>
						</div>
						<div  class="input-field col s12 m6">
							<input id="email" name="email" value="{{$user->email}}" type="text" disabled class="validate">
							<label  class="active" for="email">Email Address</label>
						</div>
						<div  class="input-field col s12 m6">
							<input id="phone" name="phone" value="{{$user->user_phone}}" type="text" required class="validate">
							<label  class="active" for="phone">Phone Number</label>
						</div>
					</div>
					<div class="divider"></div>
					<div class="row">
						<div class="col m12">
							<p class="center-align">
								<button class="btn waves-effect waves-light " type="submit" >Update Profile</button>
							</p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@stop