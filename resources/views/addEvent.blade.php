@extends('layouts.admin')
@section('title', 'Add Event')
@section('content')
<section id="content">
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
		<div class="row">
			<div class="col s12 m12 l12">
				<h5 class="breadcrumbs-title">
				User Profile
				</h5>
				<ol class="breadcrumbs">
					<li>
						<a href="">
							Dashboard
						</a>
					</li>
					<li>
						<a href="#">
							Registry
						</a>
					</li>
					<li class="active">
						Add Event
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<div class="row">
				<form class="col s12" method="POST" action="{{ url('addEvent') }}">
				{!! csrf_field() !!}
					<div class="row">
						<div  class="input-field col s12 m6">
							<input id="title" name="title" type="text" style="text-transform:uppercase" required class="validate">
							<label for="title">Event Title</label>
						</div>
						<div  class="input-field col s12 m6">
							<input id="details" name="details"  type="text" required class="validate">
							<label for="details">Event Details</label>
						</div>
						<div  class="input-field col s12 m6">
							<input id="location" name="location" type="text" required class="validate">
							<label for="location">Event Location</label>
						</div>
						<div  class="input-field col s12 m6">
							 <input id="date" type="date" name="date" required>
						</div>
					</div>
					<div class="divider"></div>
					<div class="row">
						<div class="col m12">
							<p class="center-align">
								<button class="btn waves-effect waves-light " type="submit" >Add Event</button>
							</p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@stop