@extends('layouts.admin')
@section('title', 'Fees')
@section('content')
<section id="content">
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
		<div class="row">
			<div class="col s12 m12 l12">
				<h5 class="breadcrumbs-title">
				User Profile
				</h5>
				<ol class="breadcrumbs">
					<li>
						<a href="">
							Dashboard
						</a>
					</li>
					<li>
						<a href="#">
							Students
						</a>
					</li>
					<li class="active">
						Results
					</li>
				</ol>
			</div>
		</div>
	</div>
	<table id="example" class="display" cellspacing="0" width="95%">
		<thead>
			<tr>
				<th>Unit Code</th>
				<th>Grade</th>
				<th>Unit Name</th>
				
			</tr>
		</thead>
		<tbody>
			@foreach ($results as $result)
			<tr>
				<td>{{$result->result_unitCode}}</td>
				<td>{{$result->result_grade}}</td>
				<td>{{$result->result_unitName}}</td>
				
			</tr>
			@endforeach
			
		</tbody>
	</table>
	<div class="row center-align" style='padding-top: 3em;'>
		<a class="waves-effect waves-light btn"  target="_blank" onclick="javascript:demoFromHTML();">Print PDF</a>
	</div>
</section>
@stop