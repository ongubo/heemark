@extends('layouts.master')
@section('title', 'Register-Heemark School')
@section('content')
<div class="row valign-wrapper" style="background-image:url('{{asset('assets/img/bg10.jpg')}}'); height:93%;">
	<div class="col s12 m5" style="margin-left: 25%">
		<form>
			<div class="card purple darken-1">
				<div class="card-content white-text">
					<span class="card-title">
						Register
					</span>
					<div id="basic-form" class="section">
						<div class="row">
							<div class="col s12 m12 ">
								<div class="row">
									<div class="input-field col s12">
										<input id="email" type="email">
										<label for="email" class="">Email</label>
									</div>
									<div class="input-field col s12">
										<input id="password" type="password" style="cursor: auto; background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACIUlEQVQ4EX2TOYhTURSG87IMihDsjGghBhFBmHFDHLWwSqcikk4RRKJgk0KL7C8bMpWpZtIqNkEUl1ZCgs0wOo0SxiLMDApWlgOPrH7/5b2QkYwX7jvn/uc//zl3edZ4PPbNGvF4fC4ajR5VrNvt/mo0Gr1ZPOtfgWw2e9Lv9+chX7cs64CS4Oxg3o9GI7tUKv0Q5o1dAiTfCgQCLwnOkfQOu+oSLyJ2A783HA7vIPLGxX0TgVwud4HKn0nc7Pf7N6vV6oZHkkX8FPG3uMfgXC0Wi2vCg/poUKGGcagQI3k7k8mcp5slcGswGDwpl8tfwGJg3xB6Dvey8vz6oH4C3iXcFYjbwiDeo1KafafkC3NjK7iL5ESFGQEUF7Sg+ifZdDp9GnMF/KGmfBdT2HCwZ7TwtrBPC7rQaav6Iv48rqZwg+F+p8hOMBj0IbxfMdMBrW5pAVGV/ztINByENkU0t5BIJEKRSOQ3Aj+Z57iFs1R5NK3EQS6HQqF1zmQdzpFWq3W42WwOTAf1er1PF2USFlC+qxMvFAr3HcexWX+QX6lUvsKpkTyPSEXJkw6MQ4S38Ljdbi8rmM/nY+CvgNcQqdH6U/xrYK9t244jZv6ByUOSiDdIfgBZ12U6dHEHu9TpdIr8F0OP692CtzaW/a6y3y0Wx5kbFHvGuXzkgf0xhKnPzA4UTyaTB8Ph8AvcHi3fnsrZ7Wore02YViqVOrRXXPhfqP8j6MYlawoAAAAASUVORK5CYII=&quot;); background-attachment: scroll; background-position: 100% 50%; background-repeat: no-repeat;">
										<label for="password" class="">Password</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-action center-align ">
					<a href="{{url('register')}}" class="white-text">
						Register
					</a>
					<a href="{{url('login')}}" class="white-text">
						Login
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
@stop