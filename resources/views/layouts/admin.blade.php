<html>
    <head>
        <title>@yield('title')</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{ URL::asset('assets/css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="{{ URL::asset('assets/css/materialize.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="{{ URL::asset('assets/css/jquery.dataTables.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="{{ URL::asset('assets/css/chosen.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
    </head>
    <body>
        @section('navbar')
        <header id="header" class="page-topbar">
            <div class="navbar-fixed">
                <nav class='purple darken-4'>
                    <div class="nav-wrapper">
                        <ul class="right hide-on-med-and-down">
                            <li>
                                <a href="{{url('logout')}}" class="logout waves-effect waves-block  waves-light ">
                                    <i class=" material-icons">power_settings_new</i>
                                </a>
                            </li>
                        </ul>
                        <ul id="slide-out" class="side-nav">
                            <li class="user-details purple darken-4">
                                <div class="row">
                                    <div class="col col s4 m4 l4">
                                        <i class="medium mdi-action-account-circle white-text" style='margin-top:20%;'>
                                        </i>
                                    </div>
                                    <div class="col col s8 m8 l8">
                                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">
                                        </a>
                                        <p class="user-roal">
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="bold">
                                <a href="" class="waves-effect waves-cyan">
                                    <i class="mdi-action-dashboard"></i>
                                    Dashboard
                                </a>
                            </li>
                            <li class="bold">
                                <a href="" class="waves-effect waves-cyan">
                                    <i class="mdi-action-account-circle"></i>
                                    Profile Management
                                </a>
                            </li>
                            <li class="bold">
                                <a href="" class="waves-effect waves-cyan">
                                    <i class="mdi-content-content-paste"></i>
                                    Units
                                </a>
                            </li>
                            <li class="bold">
                                <a href="" class="waves-effect waves-cyan">
                                    <i class="mdi-content-content-paste"></i>
                                    Fee Statement
                                </a>
                            </li>
                            <li class="bold">
                                <a href="{{url('logout')}}">
                                    <i class="material-icons">power_settings_new</i>
                                    Log Out
                                </a>
                            </li>
                        </ul>
                        <a href="#" data-activates="slide-out" class="button-collapse">
                            <i class="mdi-navigation-menu">
                            </i>
                        </a>
                    </div>
                </nav>
            </div>
        </header>
        <div id="main">
            <div class="wrapper">
                @show
                <aside id="left-sidebar-nav">
                    <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container ps-active-y" style="width: 240px;">
                        <li class="user-details purple darken-4" >
                            <div class="row">
                                <div class="col col s4 m4 l4">
                                    <i class="medium mdi-action-account-circle white-text" style='margin-top:30%;'>
                                    </i>
                                </div>
                                <div class="col col s8 m8 l8">
                                    <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">
                                    {{$user->user_name}} {{$user->user_lastName}}</a>
                                    <p class="user-roal" style='text-transform: capitalize;'>{{$user->user_role}}
                                    </p>
                                </div>
                            </div>
                        </li>
                        @if ($user->user_role=='Student')
                        <li class="bold">
                            <a href="{{url('user/dashboard')}}" class="waves-effect waves-cyan active">
                                <i class="mdi-action-dashboard">
                                </i>
                                Dashboard
                            </a>
                        </li>
                        <li class="bold">
                            <a href="{{url('user/units')}}" class="waves-effect waves-cyan">
                                <i class="mdi-action-done-all"></i>
                                Units
                            </a>
                        </li>
                        <li class="bold">
                            <a href="{{url('user/viewResults')}}" class="waves-effect waves-cyan">
                                <i class="mdi-content-content-paste"></i>
                                Results
                            </a>
                        </li>
                        <li class="bold">
                            <a href="{{url('user/fees')}}" class="waves-effect waves-cyan">
                                <i class="mdi-av-equalizer"></i>
                                Fee Statement
                            </a>
                        </li>
                        @endif
                        @if ($user->user_role=='Lecturer')
                        <li class="bold">
                            <a href="{{url('user/newResults')}}" class="waves-effect waves-cyan">
                                <i class="mdi-content-content-paste"></i>
                                Add Results
                            </a>
                        </li>
                        @endif
                        @if ($user->user_role=='Administrator')
                        <li class="bold">
                            <a href="{{url('user/allUsers')}}" class="waves-effect waves-cyan">
                                <i class="mdi-content-content-paste"></i>
                                Users
                            </a>
                        </li>
                        @endif
                        @if ($user->user_role=='Registry')
                        <li class="bold">
                            <a href="{{url('user/addEvent')}}" class="waves-effect waves-cyan">
                                <i class="mdi-content-content-paste"></i>
                                Add Event
                            </a>
                        </li>
                        @endif
                        @if ($user->user_role=='Finance Staff')
                        <li class="bold">
                            <a href="{{url('user/addFee')}}" class="waves-effect waves-cyan">
                                <i class="mdi-content-content-paste"></i>
                                Add/Edit Fee Records
                            </a>
                        </li>
                        <li class="bold">
                            <a href="{{url('user/manageFee')}}" class="waves-effect waves-cyan">
                                <i class="mdi-content-content-paste"></i>
                                Fee Records
                            </a>
                        </li>
                        @endif
                        <li class="bold">
                            <a href="{{url('user/profile')}}" class="waves-effect waves-cyan">
                                <i class="mdi-action-account-circle">
                                </i>
                                Profile Management
                            </a>
                        </li>
                        <li class="bold">
                            <a href="{{url('user/events')}}" class="waves-effect waves-cyan">
                                <i class="mdi-content-content-paste"></i>
                                Events
                            </a>
                        </li>
                        <li class="bold">
                            <a href="{{url('logout')}}" class="logout">
                                <i class="mdi-action-settings-power"></i>
                                Log Out
                            </a>
                        </li>
                        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
                            <div class="ps-scrollbar-x" style="left: 0px; width: 0px;">
                            </div>
                        </div>
                        <div class="ps-scrollbar-y-rail" style="top: 0px; height: 150px; right: 3px;">
                            <div class="ps-scrollbar-y" style="top: 0px; height: 19px;">
                            </div>
                        </div>
                    </ul>
                </aside>
                @yield('content')
            </div>
        </div>
        <script src="{{ URL::asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/jquery.base64.js') }}"></script>
        <script src="{{ URL::asset('assets/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/chosen.jquery.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/materialize.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/jspdf.debug.js') }}"></script>
        <script src="{{ URL::asset('assets/js/jspdf.js') }}"></script>
        <script src="{{ URL::asset('assets/js/tableExport.js') }}"></script>
        <script src="{{ URL::asset('assets/js/sprintf.js') }}"></script>
        <script src="{{ URL::asset('assets/js/base64.js') }}"></script>
        <script src="{{ URL::asset('assets/js/init.js') }}"></script>
    </body>
</html>