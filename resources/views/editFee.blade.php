@extends('layouts.admin')
@section('title', 'Fees')
@section('content')
<section id="content">
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
		<div class="row">
			<div class="col s12 m12 l12">
				<h5 class="breadcrumbs-title">
				User Profile
				</h5>
				<ol class="breadcrumbs">
					<li>
						<a href="">
							Dashboard
						</a>
					</li>
					<li>
						<a href="#">
							User
						</a>
					</li>
					<li class="active">
						Add/Edit Fee Records
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<div class="row">
				<form class="col s12" method="POST" action="{{ url('fee') }}">
				{!! csrf_field() !!}
					<div class="row">
						<div class="input-field col s12 m6">
							<select id="studentSelect" name="studentId" class="browser-default">
								<option value="" disabled selected>Select Student ID</option>
								@foreach ($allStudents as $student)
								<option value="{{$student->user_id}}">{{$student->user_id}} : {{$student->user_name}} {{$student->user_lastName}}</option>
								@endforeach
							</select>
						</div>
						<div  class="input-field col s12 m6">
							<input id="refNumber" name="refNumber" type="text" required class="validate">
							<label for="refNumber">Transaction Number</label>
						</div>
						<div class="input-field col s12 m6">
							<select id="refNo" name="transactionType">
								<option value="" disabled selected>Choose your option</option>
								<option value="RECEIPT">Receipt</option>
								<option value="ADJUSTMENT">Adjustment</option>
								<option value="HELB LOAN">Helb Loan</option>
							</select>
							<label>Transaction Type</label>
						</div>
						<div  class="input-field col s12 m6">
							<input id="amount" name="amount" type="text" required class="validate">
							<label for="amount">Amount</label>
						</div>
					</div>
					<div class="divider"></div>
					<div class="row">
						<div class="col m12">
							<p class="center-align">
								<button class="btn waves-effect waves-light " type="submit" >Add Records</button>
							</p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@stop