@extends('layouts.master')
@section('title', 'Login-Heemark')
@section('content')
<div class="row valign-wrapper" style="background-image:url('{{asset('assets/img/bg10.jpg')}}'); height:93%;">
	<div class="col s12 m5" style="margin-left: 25%">
		<form method="post" action="login">
			{!! csrf_field() !!}
			<div class="card purple darken-1">
				<div class="card-content white-text">
					<span class="card-title">
						Login
					</span>
					<div id="basic-form" class="section">
						<div class="row">
							<div class="col s12 m12 ">
								<div class="row">
									<div class="input-field col s12">
										<input id="email" type="email" name="email">
										<label for="email" class="">Email</label>
									</div>
									<div class="input-field col s12">
										<input id="password" type="password" name="password">
										<label for="password" class="">Password</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-action center-align ">
					<button class="btn white darken-4 waves-effect waves-light black-text" type="submit">Login
					<i class="mdi-content-send right"></i>
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
@stop