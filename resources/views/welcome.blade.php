@extends('layouts.master')
@section('title', 'Register-Heemark School')
@section('content')
<div class="row valign-wrapper white-text" style="height:94%; margin-bottom:-25px; background-image:url({{asset('assets/img/bg10.jpg')}});">
	<div class="row center-align">
		<h1 class="valign "><span style="font-family: 'Pacifico', cursive; font-size:larger;">Heemark School Portal</span></h1>
	</div>
</div>
@stop