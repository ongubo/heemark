@extends('layouts.admin')
@section('title', 'Results')
@section('content')
<section id="content">
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
		<div class="row">
			<div class="col s12 m12 l12">
				<h5 class="breadcrumbs-title">
				User Profile
				</h5>
				<ol class="breadcrumbs">
					<li>
						<a href="">
							Dashboard
						</a>
					</li>
					<li>
						<a href="#">
							Lecturer
						</a>
					</li>
					<li class="active">
						Add Results
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<div class="row">
				<form class="col s12" method="POST" action="{{ url('result') }}">
				{!! csrf_field() !!}
					<div class="row">
						<div class="input-field col s12 m6">
							<select id="studentSelect" name="studentId" class="browser-default">
								<option value="" disabled selected>Select Student ID</option>
								@foreach ($allStudents as $student)
								<option value="{{$student->user_id}}">{{$student->user_id}} : {{$student->user_name}} {{$student->user_lastName}}</option>
								@endforeach
							</select>
						</div>
						<div  class="input-field col s12 m6">
							<input id="unitCode" name="unitCode" type="text" style="text-transform:uppercase" required class="validate">
							<label for="unitCode">Unit Code</label>
						</div>
						<div  class="input-field col s12 m6">
							<input id="unitName" name="unitName" style="text-transform:capitalize;" type="text" required class="validate">
							<label for="unitName">Unit Name</label>
						</div>
						<div  class="input-field col s12 m6">
							<input id="grade" name="grade" type="text" style="text-transform:uppercase" required class="validate">
							<label for="grade">Grade</label>
						</div>
					</div>
					<div class="divider"></div>
					<div class="row">
						<div class="col m12">
							<p class="center-align">
								<button class="btn waves-effect waves-light " type="submit" >Add Results</button>
							</p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@stop