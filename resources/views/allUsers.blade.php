@extends('layouts.admin')
@section('title', 'Users')
@section('content')
<section id="content">
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
		<div class="row">
			<div class="col s12 m12 l12">
				<h5 class="breadcrumbs-title">
				User Profile
				</h5>
				<ol class="breadcrumbs">
					<li>
						<a href="">
							Dashboard
						</a>
					</li>
					<li>
						<a href="#">
							Administrator
						</a>
					</li>
					<li class="active">
						Manage Users
					</li>
				</ol>
			</div>
		</div>
	</div>
	<table id="example" class="display" cellspacing="0" width="90%">
		<thead>
			<tr>
				<th>First name</th>
				<th>Last name</th>
				<th>Role</th>
				<th>Email</th>
				<th>Phone Number</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($allUsers as $oneUser)
			<tr>
				<td>{{$oneUser->user_name}}</td>
				<td>{{$oneUser->user_lastName}}</td>
				<td>{{$oneUser->user_role}}</td>
				<td>{{$oneUser->email}}</td>
				<td>{{$oneUser->user_phone}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</section>
@stop