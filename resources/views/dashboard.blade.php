@extends('layouts.admin')
@section('title', 'Admin dashboard')
@section('content')
<section id="content">
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
		<div class="row">
			<div class="col s12 m12 l12">
				<h5 class="breadcrumbs-title">
				User Profile
				</h5>
				<ol class="breadcrumbs">
					<li>
						<a href="">
							Dashboard
						</a>
					</li>
					<li>
						<a href="#">
							User
						</a>
					</li>
					<li class="active">
						Home Statistics
					</li>
				</ol>
			</div>
		</div>
	</div>
	@if ($user->user_role=='Student')
	<div id="card-stats">
		<div class="row">
			<div class="col s12 m4">
				<div class="card">
					<div class="card-content  green white-text">
						<p class="card-stats-title">
							Admission Number
						</p>
						<h4 class="card-stats-number">
						{{$user->user_id}}
						</h4>
					</div>
				</div>
			</div>
			<div class="col s12 m4">
				<div class="card">
					<div class="card-content teal darken-4 white-text">
						<p class="card-stats-title">
							Units registered
						</p>
						<h4 class="card-stats-number">
						{{$unitCount}}
						</h4>
					</div>
				</div>
			</div>
			<div class="col s12 m4">
				<div class="card">
					<div class="card-content red darken-3 white-text">
						<p class="card-stats-title">
							Fee Balance
						</p>
						<h4 class="card-stats-number">
						{{$fees}}
						</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	<table id="example" class="display" cellspacing="0" width="90%">
		<thead>
			<tr>
				<th>#</th>
				<th>Unit code</th>
				<th>Unit Name</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($units as $unit)
			<tr>
				<th>#</th>
				<td>{{$unit->unit_code}}</td>
				<td>{{$unit->unit_name}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@endif
</section>
@stop