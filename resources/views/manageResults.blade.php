@extends('layouts.admin')
@section('title', 'Results')
@section('content')
<section id="content">
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
		<div class="row">
			<div class="col s12 m12 l12">
				<h5 class="breadcrumbs-title">
				User Profile
				</h5>
				<ol class="breadcrumbs">
					<li>
						<a href="">
							Dashboard
						</a>
					</li>
					<li>
						<a href="#">
							User
						</a>
					</li>
					<li class="active">
						Manage Results
					</li>
				</ol>
			</div>
		</div>
	</div>
	<table id="example" class="display" cellspacing="0" width="90%">
		<thead>
			<tr>
				<th>Student ID</th>
				<th>First name</th>
				<th>Last name</th>
				<th>Unit Code</th>
				<th>Unit Name</th>
				<th>Grade</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($results as $result)
			<tr>
				<td>{{$result->student_id}}</td>
				<td>{{$result->user->name}}</td>
				<td>{{$result->user->lastName}}</td>
				<td>{{$result->unitCode}}</td>
				<td>{{$result->unitName}}</td>
				<td>{{$result->grade}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</section>
@stop