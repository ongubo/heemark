@extends('layouts.admin')
@section('title', 'Events')
@section('content')
<section id="content">
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
		<div class="row">
			<div class="col s12 m12 l12">
				<h5 class="breadcrumbs-title">
				User Profile
				</h5>
				<ol class="breadcrumbs">
					<li>
						<a href="">
							Dashboard
						</a>
					</li>
					<li class="active">
						Events
					</li>
				</ol>
			</div>
		</div>
	</div>
	<ul class="collapsible" data-collapsible="accordion">
		@foreach ($events as $event)
		<li>
			<div class="collapsible-header">{{$event->advert_title}}</div>
			<div class="collapsible-body">
				<p>{{$event->advert_details}}</p>
				</i><br>
				<div class="chip">
				{{$event->created_at}}
				</div>
				<div class="chip">
				Location:{{$event->advert_location}}
				</div>
			</div>
		</li>
		@endforeach
	</ul>
</section>
@stop