@extends('layouts.admin')
@section('title', 'Fees')
@section('content')
<section id="content">
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
		<div class="row">
			<div class="col s12 m12 l12">
				<h5 class="breadcrumbs-title">
				User Profile
				</h5>
				<ol class="breadcrumbs">
					<li>
						<a href="">
							Dashboard
						</a>
					</li>
					<li>
						<a href="#">
							User
						</a>
					</li>
					<li class="active">
						Manage Fee Records
					</li>
				</ol>
			</div>
		</div>
	</div>
	<table id="example" class="display" cellspacing="0" width="95%">
		<thead>
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Student ID</th>
				<th>Transaction ID</th>
				<th>transaction Type</th>
				<th>Amount</th>
				<th>Transaction Date</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($fees as $fee)
		    <tr>
				<td>{{$fee->user->user_name}}</td>
				<td>{{$fee->user->user_lastName}}</td>
				<td>{{$fee->student_id}}</td>
				<td><a href="{{url('fee/'.$fee->id)}}">{{$fee->fee_referenceNumber}}</a></td>
				<td>{{$fee->fee_transactionType}}</td>
				<td>{{$fee->fee_amount}}</td>
				<td>{{$fee->created_at}}</td>
			</tr>
		@endforeach
		</tbody>

	</table>
</section>
@stop