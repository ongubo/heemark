@extends('layouts.admin')
@section('title', 'Units')
@section('content')
<section id="content">
	<link href="{{ URL::asset('assets/css/chosen.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
		<div class="row">
			<div class="col s12 m12 l12">
				<h5 class="breadcrumbs-title">
				User Profile
				</h5>
				<ol class="breadcrumbs">
					<li>
						<a href="">
							Dashboard
						</a>
					</li>
					<li>
						<a href="#">
							User
						</a>
					</li>
					<li class="active">
						Units
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12 m12 l12">
			<div class="card-panel">
				<div class="row">
					<form class="col s12" method="POST" action="{{ url('addUnit') }}">
					{!! csrf_field() !!}
						<h4 class="header2">Select A unit to Register</h4>
						<div class="row">
							<div class="input-field col s12 m6">
								<select id="studentSelect" name="studentId" class="browser-default">
									<option value="" disabled selected>Select Unit</option>
									@foreach ($courses as $course)
									<option value="{{$course->courseunit_id}}">{{$course->courseunit_code}} : {{$course->courseunit_name}}</option>
									@endforeach
								</select>
							</div>
							<div class="input-field col s6">
								<div class="input-field col s12">
									<button class="btn cyan waves-effect waves-light" type="submit" name="action"> Add Unit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<table id="example" class=" display" cellspacing="0" width="90%">
		<thead>
			<tr>
				<th>Unit code</th>
				<th>Unit Name</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($units as $unit)
			<tr>
				<td>{{$unit->unit_code}}</td>
				<td>{{$unit->unit_name}}</td>
				<!-- <td><a href="unit/{{$unit->unit_id}}" data-method="post" data-token="{{csrf_token()}}" data-confirm="Are you sure?"><i class="mdi-action-delete"></i></a></td> -->
				<td>{!! Form::open([
            'method' => 'DELETE',
            'route' => ['units.destroy', $unit->unit_id]
        ]) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<div class="row center-align" style='padding-top: 3em;'>
		<a class="waves-effect waves-light btn"  target="_blank" onclick="javascript:demoFromHTML();">Download As PDF</a>
	</div>
</section>
<script src="{{ URL::asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/chosen.jquery.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/init2.js') }}"></script>
@stop