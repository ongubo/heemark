@extends('layouts.admin')
@section('title', 'Fee statement')
@section('content')
<section id="content">
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
		<div class="row">
			<div class="col s12 m12 l12">
				<h5 class="breadcrumbs-title">
				User Profile
				</h5>
				<ol class="breadcrumbs">
					<li>
						<a href="">
							Dashboard
						</a>
					</li>
					<li>
						<a href="#">
							User
						</a>
					</li>
					<li class="active">
						Fee Statement
					</li>
				</ol>
			</div>
		</div>
	</div>
	<table id="example" class="display" cellspacing="0" width="90%">
		<thead>
			<tr>
				<th>Transaction ID</th>
				<th>Transaction Type</th>
				<th>Amount</th>
				<th>Transaction Date</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($fees as $fee)
			<tr>
				<td>{{$fee->fee_referenceNumber}}</td>
				<td>{{$fee->fee_transactionType}}</td>
				<td>{{$fee->fee_amount}}</td>
				<td>{{$fee->created_at}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<div class="row center-align" style='padding-top: 3em;'>
		<a class="waves-effect waves-light btn"  target="_blank" onclick="javascript:demoFromHTML();">Print PDF</a>
	</div>
</section>
@stop