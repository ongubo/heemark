$(document).ready(function() {
    $('#example').DataTable({
        columnDefs: [{
            targets: [0],
            orderData: [0, 1]
        }, {
            targets: [1],
            orderData: [1, 0]
        }, {
            targets: [4],
            orderData: [4, 0]
        }]
    });
    $("#studentSelect").chosen();
    $('#refNo').material_select();
    $('.collapsible').collapsible({});
    $('.select').material_select();

});

$(document).ready(function() {
        $("#unitsSelect").chosen();
    });
function demoFromHTML() {
    $('#example').tableExport({
        type: 'pdf',
        escape: 'false',
        pdfFontSize: 10
    });
}

function deleteUser(id) {
    if (confirm('Delete this user?')) {
        $.ajax({
            type: "DELETE",
            url: 'users/' + id, //resource
            success: function(affectedRows) {
                if (affectedRows > 0) window.location = 'users';
            }
        });
    }
}
function deleteUnit(id) {
    if (confirm('Delete this Unit?')) {
        $.ajax({
            type: "POST",
            url: 'units/' + id, //resource
            success: function(affectedRows) {
                alert('Deleted');
            }
        });
    }
}

