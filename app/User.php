<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
class User extends Authenticatable
{
     use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name', 'email', 'password',
    ];
    protected $primaryKey = 'user_id';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function fees()
    {
        return $this->hasMany('App\Fee', 'fee_student_id');
    }
    public function units()
    {
        return $this->hasMany('App\Unit', 'unit_student_id');
    }
    public function results()
    {
        return $this->hasMany('App\Result', 'result_student_id');
    }
}
