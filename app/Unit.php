<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
	protected $primaryKey = 'unit_id';
    public function user()
    {
        return $this->belongsTo('App\User', 'unit_student_id');
    }
}
