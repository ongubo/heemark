<?php

namespace App\Http\Controllers;

use App\Fee;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fee                  = new Fee;
        $fee->fee_student_id      = $request->input('studentId');
        $fee->fee_referenceNumber = $request->input('refNumber');
        $fee->fee_transactionType = $request->input('transactionType');
        $fee->fee_amount          = $request->input('amount');
        $fee->save();

        return redirect('user/manageFee');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $user  = auth()->user();
        // $users = User::where('role', 'Student')->get();
        // $fee   = Fee::find($id);
        // if ($id == 'add') {
        //     $fee = null;
        //     return view('editFee', [
        //         'user'  => $user,
        //         'users' => $users,
        //         'fee'   => $fee,
        //     ]);
        // }
        // return view('editFee', [
        //     'user'  => $user,
        //     'users' => $users,
        //     'fee'   => $fee,
        // ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
