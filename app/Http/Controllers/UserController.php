<?php
namespace App\Http\Controllers;

use App\Fee;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        if ($user->user_role == 'Student') {
            return redirect('user/dashboard');
        } elseif ($user->user_role == 'Lecturer') {
            return redirect('user/results');
        } elseif ($user->user_role == 'Finance Staff') {
            return redirect('user/manageFee');
        } else {
            return redirect('user/profile');
        }
    }
    public function create()
    {
    }
/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
    public function store(Request $request)
    {
        $user                = auth()->user();
        $user->user_name     = $request->input('name');
        $user->user_lastname = $request->input('lastname');
        $user->user_phone    = $request->input('phone');
        $user->save();
        return view('profile', [
            'user' => $user,
        ]);
    }
/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
    public function show($id)
    {
        $user        = auth()->user();
        $allStudents = User::where('user_role', 'Student')->get();
        if ($id == 'fees') {
            $statement = DB::table('fees')->where('fee_student_id', $user->user_id)->get();
            return view('feeStatement', [
                'user' => $user,
                'fees' => $statement]);
        } elseif ($id == 'dashboard') {
            $feeBalance = 0;
            $fees       = DB::table('fees')->where('fee_student_id', $user->user_id)->get();
            $units      = DB::table('units')->where('unit_student_id', $user->user_id)->get();
            $unitCount  = $user->units()->count();
            $feeBalance = DB::table('fees')->where('fee_student_id', $user->user_id)->sum('fee_amount');
            return view('dashboard', [
                'user'      => $user,
                'units'     => $units,
                'unitCount' => $unitCount,
                'fees'      => number_format((100000 - $feeBalance), 2, '.', ','),
            ]);
        } elseif ($id == 'viewResults') {
            $results = DB::table('results')->where('result_student_id', $user->user_id)->get();
            return view('results', [
                'user'    => $user,
                'results' => $results,
            ]);
        } elseif ($id == 'units') {
            $units   = DB::table('units')->where('unit_student_id', $user->user_id)->get();
            $courses = DB::table('courseUnits')->get();
            return view('units', [
                'user'    => $user,
                'units'   => $units,
                'courses' => $courses,
            ]);
        } elseif ($id == 'newResults') {
            return view('addResults', [
                'user'        => $user,
                'allStudents' => $allStudents,
            ]);
        } elseif ($id == 'profile') {
            return view('profile', [
                'user' => $user,
            ]);
        } elseif ($id == 'manageFee') {
            if ($user->user_role == 'Finance Staff') {
                return view('manageFee', [
                    'user' => $user,
                    'fees' => Fee::all(),
                ]);
            }
        } elseif ($id == 'addFee') {
            if ($user->user_role == 'Finance Staff') {
                $fee = null;
                return view('editFee', [
                    'user'        => $user,
                    'allStudents' => $allStudents,
                    'fee'         => $fee,
                ]);
            }
        } elseif ($id == 'manageResults') {
            if ($user->user_role == 'Lecturer') {
                $results = Result::all();
                return view('manageResults', [
                    'user'    => $user,
                    'results' => $results,
                ]);
            }
        } elseif ($id == 'addEvent') {
            if ($user->user_role == 'Registry') {
                return view('addEvent', [
                    'user' => $user,
                ]);
            }
        } elseif ($id == 'events') {
            $events = DB::table('adverts')->orderBy('created_at', 'desc')->get();
            return view('events', [
                'user'   => $user,
                'events' => $events,
            ]);
        } elseif ($id == 'allUsers') {
            if ($user->user_role == 'Administrator') {
                $allUsers = User::all();
                return view('allUsers', [
                    'user'     => $user,
                    'allUsers' => $allUsers,
                ]);
            }
        }
        return redirect('user/profile');
    }
/**
 * Show the form for editing the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
    public function edit($id)
    {
//
    }
/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
    public function update(Request $request, $id)
    {
//
    }
/**
 * Remove the specified resource from storage.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
    public function destroy($id)
    {
        $user = auth()->user();
        if ($user->user_id != $id && $user->user_role == 'Administrator') {
            $del = User::find($id);
            $del->delete();
        }
        return redirect('user/allUsers');
    }


    public function addEvent(Request $request)
    {
        DB::table('adverts')->insert(
            [
                'advert_title'    => $request->input('title'),
                'advert_details'  => $request->input('details'),
                'advert_location' => $request->input('location'),
                'advert_image'    => 'http://lorempixel.com/400/200/sports/',
                'created_at'      => $request->input('date'),
            ]
        );
        return redirect('user/events');
    }

    public function addUnit(Request $request)
    {
        $user = auth()->user();
        $unitName=DB::table('courseUnits')->where('courseunit_id', $request->input('studentId'))->first();
        DB::table('units')->insert([
            'unit_student_id' => $user->user_id,
            'unit_code'  => $unitName->courseunit_code,
            'unit_name'  =>$unitName->courseunit_name,
        ]);
        return redirect('user/units');
    }
}
