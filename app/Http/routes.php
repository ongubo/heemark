<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

// Route::get('login', function () {
//     return view('login');
// });
// Route::get('register', function () {
//     return view('register');
// });
// Route::post('login', 'AuthController@authenticate');
// Route::get('logout', 'AuthController@getLogout');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
 */
// Route::get('/', [
//        'as'   => 'profile',
//        'uses' => 'UserController@showProfile',
//    ]);
Route::get('/', function () {
    return view('welcome');
});
Route::get('logout', function () {
    Auth::logout();
});

Route::group(['middleware' => ['web']], function () {
    Route::auth();
    Route::group(['middleware' => ['auth']], function () {
        Route::resource('user', 'UserController');
        Route::resource('fee', 'FeeController');
        Route::resource('units', 'UnitController');
        Route::resource('result', 'ResultController');
        Route::post('addEvent', [
            'as'   => 'addEvent',
            'uses' => 'UserController@addEvent',
        ]);
        Route::post('addUnit', [
            'as'   => 'addUnit',
            'uses' => 'UserController@addUnit',
        ]);
    });

});
